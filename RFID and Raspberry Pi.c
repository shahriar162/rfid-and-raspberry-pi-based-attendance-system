#include<wiringPi.h>
#include<wiringSerial.h>
#include<stdio.h>
#include<string.h>
#define RS 11
#define EN 10
#define D4 6
#define D5 5
#define D6 4
#define D7 1
#define led1 23
#define led2 24
#define led3 25
#define led5 14
#define buzz 7
#define in1 21
int sp ;
int count1=0,count2=0,count3=0;
char ch;
char rfid[13];
int i=0;
char temp[5];
void check_button();
void lcdcmd(unsigned int ch){
int temp=0x80;
digitalWrite(D4, temp & ch<<3);
digitalWrite(D5, temp & ch<<2);
digitalWrite(D6, temp & ch<<1);
digitalWrite(D7, temp & ch);
digitalWrite(RS, LOW);
digitalWrite(EN, HIGH);
delay(10);
digitalWrite(EN, LOW);
digitalWrite(D4, temp & ch<<7);
digitalWrite(D5, temp & ch<<6);
digitalWrite(D6, temp & ch<<5);
digitalWrite(D7, temp & ch<<4);
digitalWrite(RS, LOW);
digitalWrite(EN, HIGH);
delay(10);
digitalWrite(EN, LOW);}
void write(unsigned int ch){
int temp=0x80;
digitalWrite(D4, temp & ch<<3);
digitalWrite(D5, temp & ch<<2);
digitalWrite(D6, temp & ch<<1);
digitalWrite(D7, temp & ch);
digitalWrite(RS, HIGH);
digitalWrite(EN, HIGH);
delay(10);
digitalWrite(EN, LOW);
digitalWrite(D4, temp & ch<<7);
digitalWrite(D5, temp & ch<<6);
digitalWrite(D6, temp & ch<<5);
digitalWrite(D7, temp & ch<<4);

digitalWrite(RS, HIGH);
digitalWrite(EN, HIGH);
delay(10);
digitalWrite(EN, LOW);}
void clear(){
lcdcmd(0x01);}
void setCursor(int x, int y){
int set=0;
if(y==0)
set=128+x;
if(y==1)
set=192+x;
lcdcmd(set);}
void print(char *str){
while(*str){
write(*str);
str++;}}
void begin(int x, int y){
lcdcmd(0x02);
lcdcmd(0x28);
lcdcmd(0x06);
lcdcmd(0x0e);
lcdcmd(0x01);}
void buzzer(){
digitalWrite(buzz, HIGH);
delay(1000);
digitalWrite(buzz, LOW);
}
void wait(){
digitalWrite(led5, LOW);
delay(3000);
}
void serialbegin(int baud){
if ((sp = serialOpen ("/dev/ttyS0",baud)) < 0)
{
clear();
print("Unable to open");
setCursor(0,1);
print("serial Port");
}}
void setup(){
if (wiringPiSetup () == -1){
clear();
print("Unable to start");
setCursor(0,1);
print("wiringPi");}
pinMode(led1, OUTPUT);
pinMode(led2, OUTPUT);
pinMode(led3, OUTPUT);
pinMode(led5, OUTPUT);
pinMode(buzz, OUTPUT);
pinMode(RS, OUTPUT);
pinMode(EN, OUTPUT);
pinMode(D4, OUTPUT);
pinMode(D5, OUTPUT);
pinMode(D6, OUTPUT);
pinMode(D7, OUTPUT);
pinMode(in1, INPUT);
digitalWrite(in1, HIGH);
begin(16,2);
serialbegin(9600);}
void get_card(){
digitalWrite(led5, HIGH);
i=0;
while(i<12){
check_button();
while(serialDataAvail (sp)){
ch= serialGetchar(sp);
rfid[i]=ch;
fflush (stdout) ;
i++;}}
rfid[i]='\0';
buzzer();
return;}
void main(){
setup();
clear();
print("Attendance Systm");
setCursor(0,1);
print("using RPI ");
delay(2000);
clear();
print("CSE323");
setCursor(0,1);
print("Welcomes you");
delay(2000);
clear();
print("System Ready");
delay(1000);
clear();
while(1){
clear();
print("Place Your Card:");
get_card();
setCursor(0,1);
print(rfid);
delay(1000);
if(strncmp(rfid,"0900711B6003",12)==0){
count1++;
clear();
print("Attd. Registered");
setCursor(0,1);
print("Student 1");
digitalWrite(led1, HIGH);
buzzer();
digitalWrite(led1, LOW);
wait();}
else if(strncmp(rfid,"090070FE6EE9",12)==0){
count2++;
clear();
print("Attd. Registered");
setCursor(0,1);
print("Student 2");
digitalWrite(led2, HIGH);
buzzer();
digitalWrite(led2, LOW);
wait();}
else if(strncmp(rfid,"3F0072C049C4",12)==0){
count3++;
clear();
print("Attd. Registered");
setCursor(0,1);
print("Student 3");
digitalWrite(led3, HIGH);
buzzer();
digitalWrite(led3, LOW);
wait();}
else{
clear();
print("Invalid Card");
buzzer();
buzzer();
wait();}}}
void check_button(){
if(digitalRead(in1)==0){
digitalWrite(led5, LOW);
clear();
setCursor(0,0);
print("std1  std2  std3");
setCursor(1,1);
sprintf(temp,"%d",count1);
print(temp);
setCursor(7,1);
sprintf(temp,"%d",count2);
print(temp);
setCursor(13,1);
sprintf(temp,"%d",count3);
print(temp);
delay(5000);
digitalWrite(led5, HIGH);
clear();
print("Place Your Card:");}}

